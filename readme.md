# List and Open Media

FF extension to list media elements' sources on pages.

https://addons.mozilla.org/en-US/firefox/addon/list-and-open-media/

## License

Unlicense
