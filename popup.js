browser.tabs.executeScript({
	file: 'content.js',
	allFrames: true, // doesn't work due to lacking host permissions.
})
const audioList = document.querySelector('.audio-list')
const videoList = document.querySelector('.video-list')
browser.runtime.onMessage.addListener(message => {
	message.audios.forEach(url => {
		if ( !url ) {
			return
		}
		const link = document.createElement('a')
		link.href = url
		link.innerText = url

		audioList.appendChild(listElement(link));
	})
	message.videos.forEach(url => {
		if ( !url ) {
			return
		}
		const link = document.createElement('a')
		link.href = url
		link.innerText = url

		videoList.appendChild(listElement(link));
	})
})

function listElement(content) {
	const li = document.createElement('li')
	li.append(content)
	return li
}
