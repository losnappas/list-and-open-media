{
	const elements = {
		audios: [...document.querySelectorAll('audio')].map(e => e.src),
		videos: [...document.querySelectorAll('video')].map(e => e.src),
	}
	browser.runtime.sendMessage(elements);
}
